This is a Unity on Android project.
It can be used to check Rtsp Streaming latency by comparing clock ticks between TX side and RX side.
If the network speed is good, the latency is usually < 1000 ms.
