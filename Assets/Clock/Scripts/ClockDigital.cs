using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ClockDigital : MonoBehaviour
{
    private Text textClock;

    void Awake()
    {
        textClock = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        DateTime time = DateTime.Now;
        string hour = LeadingZero( time.Hour );
        string minute = LeadingZero( time.Minute );
        string second = LeadingZero( time.Second );
        string millisecond = LeadingZero( time.Millisecond );

        textClock.text = hour + ":" + minute+ ":" + second + ":"+ millisecond;

        string LeadingZero(int n)
        {
            return n.ToString().PadLeft(2,'0');
        }

    }

    // Start is called before the first frame update
    void Start()
    {

    }

}
