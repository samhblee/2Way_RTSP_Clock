using System;
using UnityEngine;
using UnityEngine.Android;

namespace RtspUpstreamer.Scripts 
{

        public class RtspUpstreamer : MonoBehaviour
        {
            public int displayID;
            public String id = "";
            public String pwd = "";
            public String rtsplink = "";
            public bool playOnStart = false;

            private String pkgName1 = "com.coretronic.unity.unitystreamexample.RtspFromUnityActivity";
            private String pkgName2 = "com.coretronic.unity.unityplugin.RtspFromUnityHelper";
            private AndroidJavaObject _androidJavaPlugin = null;


        void Start () 
        {
            if (Application.platform == RuntimePlatform.Android) {
                Permission.RequestUserPermission(Permission.Microphone);
                Permission.RequestUserPermission(Permission.Camera);
                if (playOnStart && rtsplink != null && rtsplink != "")
                {
                    UpstreamStart2();
                }

                // CreateTextureAndPassToPlugin ();
                // yield return StartCoroutine ("CallPluginAtEndOfFrames");
            }
        }

        AndroidJavaClass _class;
        AndroidJavaObject _jInstance { get { return _class.GetStatic<AndroidJavaObject>("helperInstance"); } }
        void UpstreamStart2()
        {
            _class = new AndroidJavaClass(pkgName2);
            _class.CallStatic("csStartRtspHelper", gameObject.name);

            _jInstance.Call("csStartRtp", displayID, id, pwd, rtsplink);
            //csStartRtp(int displayID, String id, String pwd, String rtsplink)
        }

        private void UpstreamStart()
        {
            using (AndroidJavaClass javaClass =
                new AndroidJavaClass(pkgName1))
            {
                _androidJavaPlugin = javaClass.GetStatic<AndroidJavaObject>("_context");
            }

            _androidJavaPlugin.Call("csStartRtp", displayID, id, pwd,
                rtsplink);
        }

        private void Awake()
        {
            
        }
    }
}