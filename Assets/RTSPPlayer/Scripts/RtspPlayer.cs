using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace Coretronic.AIRD {
    public struct DecoderInfo {
        public int channels;    // number of audio channels，通常為2
        public int frameSize;   // audio frame size，在AAC編碼時通常為1024
        public int sampleRate;  // audio sample rate，可能是48000、44100、22050等
        public int width;
        public int height;
    }

    public class RtspPlayer : MonoBehaviour
    {
        const string LIBNAME = "rtsp";

        public delegate void VideoCallback(double pts);
        public delegate void AudioCallback(double pts);
        public delegate void LogCallback(IntPtr request, int color, int size);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void RegisterDebugCallback(LogCallback cb);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern DecoderInfo RTSP_player_init([MarshalAs(UnmanagedType.LPStr)] string url);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int RTSP_player_readframe(
            VideoCallback vc, AudioCallback ac, byte[] vbuf, float[] abuf);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int RTSP_player_readaudio(AudioCallback ac, float[] abuf);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void RTSP_player_deinit();

        enum Color { red, green, blue, black, white, yellow, orange };

        //--------------------------------------------------------------------
        // 控制 player thread 的變數
        // playerThreadReading 只有在 read_frame 迴圈中時才會是true，離開後馬上變false
        //              Update()如果在 playerThreadReading==false 時，不會顯示任何內容
        // playerThreadStop 用來通知 player thread 該停止了，會用在 Disable() 時
        // info 是影像解碼後的基本資料：寬度、高度、sample rate...等
        //--------------------------------------------------------------------
        bool playerThreadStop = true, playerThreadRunning;
        bool playerThreadReading = false;
        Thread playerThread;
        DecoderInfo info;
        DateTime startTime;
        //--------------------------------------------------------------------
        // 定義 video/audio buffer
        // videoWriteIndex,  代表下一次要寫入的 buffer index
        // videoReadIndex 代表下一次要讀取的 buffer index
        //--------------------------------------------------------------------
        byte[][] videoBuffer = new byte[20][];
        int videoReadIndex = 0;
        int videoWriteIndex = 0;
        double[] videoTimestamp = new double[20];
        float videoStartTime;
        float[][] audioBuffer = new float[80][];
        int audioReadIndex = 0;
        int audioReadSubIndex = 0;
        int audioWriteIndex = 0;
        double[] audioTimestamp = new double[80];
        //--------------------------------------------------------------------
        // 其他元素
        //--------------------------------------------------------------------
        Texture2D videoTexture = null;
        RawImage rtspVideoImage = null;
        AudioSource rtspAudioSource = null;
        AudioClip rtspAudioClip = null;
        //--------------------------------------------------------------------
        // 在 Unity Editor 中可控制的變數
        //--------------------------------------------------------------------
        public String url = "";
        public bool playOnStart = false;

        static void OnDebugCallback(IntPtr request, int color, int size)
        {
            string debug_string = Marshal.PtrToStringAnsi(request, size);
            debug_string = String.Format("{0}{1}{2}{3}{4}", "<color=", ((Color)color).ToString(),
                ">", debug_string, "</color>");
            Debug.Log(debug_string);
        }

        void OnVideoFrame(double pts)
        {
            videoTimestamp[videoWriteIndex % videoBuffer.Length] = pts;
            videoWriteIndex += 1;
        }

        void OnAudioFrame(double pts)
        {
            audioTimestamp[audioWriteIndex % audioBuffer.Length] = pts;
            audioWriteIndex += 1;
            Debug.Log("audio pts=" + pts + " r=" + audioReadIndex + " w=" + audioWriteIndex);
        }

        void PlayerThreadLoop()
        {
            playerThreadRunning = true;

            // [1] 每5秒試著連線到 url，讓ffmpeg偵測URL的內容是否可解碼
            //     info.frameSize <= 0 可能是連線失敗或無法解碼
            //     如果沒有明確設 playerThreadStop=true，就繼續retry
            while (!playerThreadStop && info.frameSize <= 0) {
                Thread.Sleep(100);
                if ((DateTime.Now - startTime).Seconds > 5) {
                    info = RTSP_player_init(url);
                    startTime = DateTime.Now;
                }
            }

            // [2] 根據解碼得到的寬度、高度，配置記憶體給 video/audio buffer
            VideoCallback vc = null;
            AudioCallback ac = null;
            if (!playerThreadStop) {
                if (info.width > 0) {
                    vc = new VideoCallback(OnVideoFrame);
                    for (int i=0; i<videoBuffer.Length; i++) {
                        videoBuffer[i] = new byte[info.width * info.height * 3];
                    }
                }
                if (info.frameSize > 0) {
                    ac = new AudioCallback(OnAudioFrame);
                    for (int i=0; i<audioBuffer.Length; i++) {
                        audioBuffer[i] = new float[info.channels * info.frameSize];
                    }
                }
            }

            // 3. 讀取video/audio frame
            //    每呼叫一次讀一個packet，裡面通常只會包含一個video frame，但可能有多個audio frame
            playerThreadReading = true;
            int errorCount = 0;
            while (!playerThreadStop) {
                if (info.width <= 0) {
                    float[] abuf = audioBuffer[audioWriteIndex % audioBuffer.Length];
                    if (RTSP_player_readaudio(ac, abuf) < 0) {
                        errorCount++;
                        if (errorCount > 5) break;
                        Thread.Sleep(100);
                        continue;  // 錯誤也不會停止，除非用 playerThreadStop=true 強制停止，否則會繼續讀
                    }
                }
                else {
                    byte[] vbuf = videoBuffer[videoWriteIndex % videoBuffer.Length];
                    float[] abuf = audioBuffer[audioWriteIndex % audioBuffer.Length];
                    if (RTSP_player_readframe(vc, ac, vbuf, abuf) < 0) {
                        errorCount++;
                        if (errorCount > 5) break;
                        Thread.Sleep(100);
                        continue;  // 錯誤也不會停止，除非用 playerThreadStop=true 強制停止，否則會繼續讀
                    }
                }
            }
            playerThreadReading = false;

            // [4] 釋放資源
            for (int i=0; i<videoBuffer.Length; i++) {
                videoBuffer[i] = null;
            }
            for (int i=0; i<audioBuffer.Length; i++) {
                audioBuffer[i] = null;
            }
            RTSP_player_deinit();
            playerThreadRunning = false;
        }

        void OnAudioRead(float[] data)
        {
            if (audioWriteIndex - audioReadIndex <= 1) {
                for (int i=0; i<data.Length; i++) data[i] = 0;
                Debug.LogError("audio buffer underflow");
                return;
            }

            float[] abuf = audioBuffer[audioReadIndex % audioBuffer.Length];
            for (int i=0; i<data.Length; i++) {
                data[i] = abuf[audioReadSubIndex++];
                if (audioReadSubIndex >= abuf.Length) {
                    audioReadIndex += 1;
                    audioReadSubIndex = 0;
                    abuf = audioBuffer[audioReadIndex % audioBuffer.Length];
                }
            }
        }

        void Update()
        {
            if (!playerThreadReading) return;
            if (info.frameSize <= 0) return;

            if (info.width > 0 && videoTexture == null) {
                videoTexture = new Texture2D(info.width, info.height, TextureFormat.RGB24, false);
                rtspVideoImage.texture = videoTexture as Texture;
                rtspVideoImage.enabled = true;
            }

            if (rtspAudioClip == null) {
                rtspAudioClip = AudioClip.Create("rtsp_audio", info.frameSize,
                    info.channels, info.sampleRate, true, OnAudioRead);
                rtspAudioSource.clip = rtspAudioClip;
                rtspAudioSource.loop = true;
            }

            if (info.width <= 0 && info.frameSize > 0) {
                rtspVideoImage.enabled = false;
                if (!rtspAudioSource.isPlaying) rtspAudioSource.Play();
                return;
            }

            if (rtspAudioSource.isPlaying && !playerThreadRunning) {
                Stop();
            }

            // buffer underflow 時，重播4個frame，使buffer保持15個frame
            if (videoWriteIndex - videoReadIndex <= 11 && videoReadIndex > 5) {
                Debug.LogWarning("video buffer underflow  r=" + videoReadIndex + " w=" + videoWriteIndex);
                videoReadIndex -= 4;
                videoStartTime = Time.time - (float)videoTimestamp[videoReadIndex % videoBuffer.Length] - 0.001f;
            }

            // buffer overflow 時，省略4個frame，使buffer保持15個frame
            if (videoWriteIndex - videoReadIndex >= 19) {
                Debug.LogWarning("video buffer overflow  r=" + videoReadIndex + " w=" + videoWriteIndex);
                videoReadIndex += 4;
                videoStartTime = Time.time - (float)videoTimestamp[videoReadIndex % videoBuffer.Length] - 0.001f;
            }

            if (videoReadIndex == 0) {
                if (videoWriteIndex <= 15) {
                    videoStartTime = Time.time - (float)videoTimestamp[0];
                }
            }

            // Time.time是即將顯示的game frame的時間，我們希望video frame能與game frame同步
            // videoTimestamp[] 是每個video frame應該顯示的時間，videoStartTime 是video/game的誤差時間
            // 下面的邏輯是：若game frame時間剛好超過video frame的時間，才能將video frame更新到texture
            if (videoWriteIndex > videoReadIndex &&
                (Time.time >= (videoTimestamp[videoReadIndex % videoBuffer.Length] + videoStartTime))) {
                videoTexture.LoadRawTextureData(videoBuffer[videoReadIndex % videoBuffer.Length]);
                videoTexture.Apply();
                videoReadIndex += 1;
                if (!rtspAudioSource.isPlaying) rtspAudioSource.Play();
            }
        }

        void Awake()
        {
            rtspVideoImage = gameObject.GetComponent<RawImage>();
            rtspAudioSource = gameObject.GetComponentInChildren<AudioSource>();
            for (int i=0; i<videoTimestamp.Length; i++) {
                videoTimestamp[i] = 1e8;
            }
            for (int i=0; i<audioTimestamp.Length; i++) {
                audioTimestamp[i] = 1e8;
            }
        }

        public void Stop()
        {
            if (rtspAudioSource != null) {
                rtspAudioSource.Stop();
                rtspAudioSource.clip = null;
                Destroy(rtspAudioClip);
                rtspAudioClip = null;
            }
            if (playerThread != null) {
                playerThreadStop = true;
                playerThread.Join();
            }
            if (videoTexture != null) {
                rtspVideoImage.texture = null;
                Destroy(videoTexture);
                videoTexture = null;
            }
        }

        public void Play(string urlToPlay)
        {
            if (playerThreadRunning) {  // 表示正在播放中
                Stop();
            }
            if (urlToPlay != "") {
                url = urlToPlay;
            }
            playerThreadStop = false;
            videoStartTime = 0;
            info.width = info.height = 0;
            audioWriteIndex = audioReadIndex = videoWriteIndex = videoReadIndex = 0;
            playerThread = new Thread(PlayerThreadLoop);
            playerThread.Start();
        }

        void Start()
        {
            if (playOnStart && url != null && url != "") {
                StartCoroutine(DelayedCoroutine());
            }
        }

            
        IEnumerator DelayedCoroutine()
        {
            //Print the time of when the function is first called.
            Debug.Log("Started Coroutine at timestamp : " + Time.time);

            //yield on a new YieldInstruction that waits for 5 seconds.
            yield return new WaitForSeconds(5);

            //After we have waited 5 seconds print the time again.
            Debug.Log("Finished Coroutine at timestamp : " + Time.time);
            Play(url);
        }

        void OnEnable()
        {
            RegisterDebugCallback(new LogCallback(OnDebugCallback));
        }

        void OnDisable()
        {
            Stop();
        }
    }
}
