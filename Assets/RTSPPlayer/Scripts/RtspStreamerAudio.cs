using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

namespace Coretronic.AIRD {
    public class RtspStreamerAudio : MonoBehaviour
    {
        const string LIBNAME = "rtsp";

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern EncoderInfo RTSP_streamer_init(
            [MarshalAs(UnmanagedType.LPStr)] string url,
            int width, int height, int fps, int channels, int sample_rate);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void RTSP_streamer_write_video(byte[] video_buffer,
            int width, int height, int dtsMilliseconds);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void RTSP_streamer_write_audio(float[] audio_buffer);

        [DllImport (LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void RTSP_streamer_deinit();

        //--------------------------------------------------------------------
        // 在 Unity Editor 中可控制的變數
        //--------------------------------------------------------------------
        public bool recordOnAwake = false;  // 是否啟動後立即產生串流
        public string streamName = "bob";  // 要產生的串流名稱
        //--------------------------------------------------------------------
        // Streamer Thread
        //--------------------------------------------------------------------
        Thread streamerThread = null;
        EncoderInfo info;
        DateTime startTime;
        bool streamerThreadStop;
        //--------------------------------------------------------------------
        // Video Buffer and Audio Buffer
        //--------------------------------------------------------------------
        string targetUrl = null;
        bool enableRecord;
        AudioClip micAudioClip = null;
        float[] audioBuffer;
        int audioWriteIndex, audioReadIndex;
        int audioChannels, audioFrequency;
        int lastSampleIndex;

        void CopyMicToBuffer()
        {
            if (lastSampleIndex == -1) {
                lastSampleIndex = Microphone.GetPosition(null);
                return;
            }

            int newSampleIndex = Microphone.GetPosition(null);
            if (newSampleIndex < lastSampleIndex) {
                int len1 = micAudioClip.samples - lastSampleIndex;
                if (len1 > 0) {
                    float[] sample1 = new float[len1 * audioChannels];
                    micAudioClip.GetData(sample1, lastSampleIndex);
                    for (int i=0; i<sample1.Length; i++) {
                        audioBuffer[audioWriteIndex % audioBuffer.Length] = sample1[i];
                        audioWriteIndex++;
                    }
                }
                if (newSampleIndex > 0) {
                    float[] sample2 = new float[newSampleIndex * audioChannels];
                    micAudioClip.GetData(sample2, 0);
                    for (int i=0; i<sample2.Length; i++) {
                        audioBuffer[audioWriteIndex % audioBuffer.Length] = sample2[i];
                        audioWriteIndex++;
                    }
                }
            }
            else if (newSampleIndex > lastSampleIndex) {
                float[] samples = new float[(newSampleIndex - lastSampleIndex) * audioChannels];
                micAudioClip.GetData(samples, lastSampleIndex);
                for (int i=0; i<samples.Length; i++) {
                    audioBuffer[audioWriteIndex % audioBuffer.Length] = samples[i];
                    audioWriteIndex++;
                }
            }
            lastSampleIndex = newSampleIndex;
        }

        void StreamerAudioOnlyThreadLoop()
        {
            startTime = DateTime.Now;
            // [1] 大約每5秒重新連線一次
            while (!streamerThreadStop && info.audio_frame_size <= 0) {
                Thread.Sleep(100);
                if ((DateTime.Now - startTime).Seconds > 5) {
                    info = RTSP_streamer_init(targetUrl, 0, 0, 0, audioChannels, audioFrequency);
                    startTime = DateTime.Now;
                }
            }

            // [2] 開始送出 audio frame
            while (!streamerThreadStop)
            {
                // 當資料量足夠送 audio frame 時，就會一直送，直到資料量不足為止
                while (audioWriteIndex - audioReadIndex >= info.audio_channels * info.audio_frame_size) {
                    float[] aud = new float[info.audio_frame_size * info.audio_channels];
                    for (int i=0; i < info.audio_frame_size * info.audio_channels; i++, audioReadIndex++) {
                        aud[i] = audioBuffer[audioReadIndex % audioBuffer.Length];
                    }
                    RTSP_streamer_write_audio(aud);
                }
            }

            // [3] 結束
            if (info.audio_frame_size > 0) {
                RTSP_streamer_deinit();
            }
        }

        void PrepareAudio()
        {
            // 麥克風名稱 null 表示使用預設麥克風
            micAudioClip = Microphone.Start(null, true, 5, 48000);
            audioChannels = micAudioClip.channels;
            audioFrequency = micAudioClip.frequency;
            audioBuffer = new float[5 * audioFrequency * audioChannels];
            audioReadIndex = audioWriteIndex = 0;
            lastSampleIndex = -1;
            Debug.Log("microphone freq=" + audioFrequency + " channels=" + audioChannels);
        }

        public void Record(string url)
        {
            targetUrl = url;
            enableRecord = true;

            streamerThread = new Thread(StreamerAudioOnlyThreadLoop);
            streamerThread.Start();
        }

        void Start()
        {
            if (recordOnAwake) {
                Record("rtsp://nchc:nchc@140.110.17.180:1935/nchc_live/" + streamName);
            }
        }

        // Vuforia 不能使用這個版本，因為 Vuforia 不會觸發 OnPostRender()
        void OnPostRender()
        {
            if (!enableRecord) return;

            if (micAudioClip == null) {
                PrepareAudio();
            }

            if (info.audio_frame_size <= 0) {
                return;  // RTSP_streamer_init 還沒有執行過，或執行失敗，不用作後續的事
            }

            // 將麥克風的資料複製到 audioBuffer
            // RTSP streamer 每次送 audio packet 需要1024筆的資料 (AAC規格)
            // 但是麥克風每次的資料量不是1024的倍數，所以要先累積在 audioBuffer 中
            CopyMicToBuffer();
        }

        public void StopRecord()  // Stop Recoding
        {
            enableRecord = false;

            Microphone.End(null);
            if (streamerThread != null) {
                streamerThreadStop = true;
                streamerThread.Join();
            }
        }

        void OnDisable()
        {
            StopRecord();
        }
    }
}
